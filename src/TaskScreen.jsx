import React from "react";
import CheckIcon from "@material-ui/icons/Check";
import { ListItem, IconButton } from "@material-ui/core";

import InputBar from "./InputBar";
import "./App.css";

const TaskScreen = ({ list, onDone, onAdd }) => {
  return (
    <>
      <InputBar onClick={onAdd} />
      <div className="list">
        {list.map((el) => (
          <ListItem className="task" key={el}>
            <span>{el}</span>
            <IconButton onClick={() => onDone(el)}>
              <CheckIcon className="check-icon" fontSize="large" />
            </IconButton>
          </ListItem>
        ))}
      </div>
    </>
  );
};

export default TaskScreen;
