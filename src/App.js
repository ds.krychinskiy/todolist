import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";

import Header from "./Header";
import Done from "./Done";
import Modal from "./Modal";
import TaskScreen from "./TaskScreen.jsx";
import "./App.css";

const App = () => {
  const [tasklist, setTaskList] = useState([]);
  const [selectedName, setSelectedName] = useState("");
  const [doneList, setDoneList] = useState([]);
  const [open, setOpen] = useState(false);

  const onDone = (name) => {
    setDoneList([...doneList, name]);
    setTaskList([]);
    const I = [...tasklist].filter((el) => {
      return el !== name;
    });
    setTaskList(I);
  };

  const onRemoveComplete = (name) => {
    const oldArr = [...doneList];
    const newArray = oldArr.filter((el) => {
      return el !== name;
    });
    setDoneList(newArray);
  };

  const onAdd = (name) => {
    const newArray = [...tasklist, name];
    setTaskList(newArray);
  };

  const onRemove = (name) => {
    setSelectedName(name);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setSelectedName("");
  };

  return (
    <>
      <Modal
        open={open}
        handleClose={handleClose}
        onRemove={() => onRemoveComplete(selectedName)}
      />
      <div className="app">
        <Header />
        <div>
          <Switch>
            <Route path="/" exact>
              <TaskScreen list={tasklist} onDone={onDone} onAdd={onAdd} />
            </Route>
            <Route path="/Done">
              <Done onRemove={onRemove} list={doneList} />
            </Route>
          </Switch>
        </div>
      </div>
    </>
  );
};

export default App;
