import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const InputBar = (props) => {
  const [value, setValue] = useState("");
  const onClick = () => {
    props.onClick(value);
    setValue("");
  };

  return (
    <div className="input-bar">
      <TextField
        className="input"
        a
        label="Quest"
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
        }}
      />
      <Button className="save-btn" onClick={onClick}>
        <p>Save</p>
      </Button>
    </div>
  );
};

export default InputBar;
