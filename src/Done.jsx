import React from "react";
import { ListItem, IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const Done = (props) => {
  const { list, onRemove } = props;
  return (
    <div className="done">
      {list.map((name) => (
        <ListItem className="task" key={name}>
          <span>{name}</span>
          <IconButton onClick={() => onRemove(name)}>
            <DeleteIcon className="delete-icon" fontSize="large" />
          </IconButton>
        </ListItem>
      ))}
    </div>
  );
};

export default Done;
