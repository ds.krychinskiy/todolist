import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <Link to="/">Topical</Link>
      <Link to="/Done">Done</Link>
    </header>
  );
};

export default Header;
