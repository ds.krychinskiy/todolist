import React from "react";
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@material-ui/core";

const Modal = (props) => {
  const { handleClose, onRemove, open } = props;
  const onRemoveClick = () => {
    onRemove();
    handleClose();
  };
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogContent>
        <DialogContentText>
          Are you sure you want to delete the entry?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onRemoveClick}>Yes</Button>
        <Button onClick={handleClose}>Canсel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default Modal;
